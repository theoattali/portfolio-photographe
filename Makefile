ifndef SYMFONY_ENV
	export SYMFONY_ENV:=dev
endif

all: composer resetdb

cc: cacheclear
cacheclear:
	php bin/console cache:clear
	rm -f php var/logs/$(SYMFONY_ENV).log

resetdb: dropdb createdb fixtures

createdb:
	php bin/console doctrine:database:create
	php bin/console doctrine:schema:create

dropdb:
	-php bin/console doctrine:database:drop --force

fixtures:
	php bin/console doctrine:fixtures:load --no-interaction


phpunit:
	php bin/phpunit -c app


test: SYMFONY_ENV:=test
test: cacheclear resetdb phpunit


composer:
	composer install --prefer-dist
