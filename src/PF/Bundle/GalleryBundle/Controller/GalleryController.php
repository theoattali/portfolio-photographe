<?php

namespace PF\Bundle\GalleryBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use PF\Bundle\GalleryBundle\Entity\Gallery;
use PF\Bundle\GalleryBundle\Form\Type\GalleryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class GalleryController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $galleries = $em->getRepository('PFGalleryBundle:Gallery')->findAll();

        return $this->render('PFGalleryBundle:Gallery:index.html.twig', array(
            'galleries' => $galleries,
        ));
    }
}
